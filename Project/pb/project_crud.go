package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/protos/types"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProjectsServiceProjectServerCrud struct {
	store ProjectStore
	bloc  ProjectsServiceProjectServerBLoC
}

type ProjectsServiceProjectServerBLoC interface {
	CreateProjectBLoC(context.Context, *CreateProjectRequest) error

	GetProjectBLoC(context.Context, *GetProjectRequest) error

	UpdateProjectBLoC(context.Context, *UpdateProjectRequest) error

	DeleteProjectBLoC(context.Context, *DeleteProjectRequest) error

	BatchGetProjectBLoC(context.Context, *BatchGetProjectRequest) error

	ListProjectBLoC(context.Context, *ListProjectRequest) (ProjectCondition, error)
}

func NewProjectsServiceProjectServerCrud(s ProjectStore, b ProjectsServiceProjectServerBLoC) *ProjectsServiceProjectServerCrud {
	return &ProjectsServiceProjectServerCrud{store: s, bloc: b}
}

func (s *ProjectsServiceProjectServerCrud) CreateProject(ctx context.Context, in *CreateProjectRequest) (*Project, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Project.Id) != in.Project.GetPrefix() {
		in.Project.Id = in.Parent
	}

	ids, err := s.store.CreateProjects(ctx, in.Project)
	if err != nil {
		return nil, err
	}

	in.Project.Id = ids[0]

	// meta info
	meta := &MetaInfo{}
	if _, err := s.store.GetProject(ctx, []string{"id"},
		ProjectIdEq{
			Id: in.GetProject().GetId(),
		}, meta,
	); err != nil {
		return nil, err
	}

	in.Project.CreatedBy = meta.CreatedBy
	in.Project.UpdatedBy = meta.UpdatedBy

	in.Project.CreatedOn = &meta.CreatedOn
	in.Project.UpdatedOn = &meta.UpdatedOn

	return in.GetProject(), nil
}

func (s *ProjectsServiceProjectServerCrud) UpdateProject(ctx context.Context, in *UpdateProjectRequest) (*Project, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetProject().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateProject(ctx,
		in.Project, mask,
		ProjectIdEq{Id: in.Project.Id},
	); err != nil {
		return nil, err
	}

	// meta infos
	meta := &MetaInfo{}

	updatedProject, err := s.store.GetProject(ctx, []string{},
		ProjectIdEq{
			Id: in.GetProject().GetId(),
		},
		meta,
	)
	if err != nil {
		return nil, err
	}

	updatedProject.CreatedBy = meta.CreatedBy
	updatedProject.UpdatedBy = meta.UpdatedBy

	updatedProject.CreatedOn = &meta.CreatedOn
	updatedProject.UpdatedOn = &meta.UpdatedOn

	return updatedProject, nil
}

func (s *ProjectsServiceProjectServerCrud) GetProject(ctx context.Context, in *GetProjectRequest) (*Project, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	meta := &MetaInfo{}
	res, err := s.store.GetProject(ctx, mask, ProjectIdEq{Id: in.Id}, meta)
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Project not found")
		}
		return nil, err
	}

	res.CreatedBy = meta.CreatedBy
	res.UpdatedBy = meta.UpdatedBy

	res.CreatedOn = &meta.CreatedOn
	res.UpdatedOn = &meta.UpdatedOn

	return res, nil
}

func (s *ProjectsServiceProjectServerCrud) ListProject(ctx context.Context, in *ListProjectRequest) (*ListProjectResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	page, err := s.GetPagedCondition(ctx, in.First, in.After, in.Last, in.Before)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithPagination(ctx, page, condition, mask)
}

func (s *ProjectsServiceProjectServerCrud) ListWithPagination(ctx context.Context, page *CursorBasedPagination, condition ProjectCondition, viewMask []string) (*ListProjectResponse, error) {

	meta := MetaInfoForList{}
	list, err := s.store.ListProjects(ctx,
		viewMask,
		condition,
		page,
		&meta,
	)
	if err != nil {
		return nil, err
	}

	res := &ListProjectResponse{
		PageInfo: &types.PageInfo{},
	}

	for i, it := range list {

		it.CreatedBy = meta[i].CreatedBy
		it.UpdatedBy = meta[i].UpdatedBy

		it.CreatedOn = &meta[i].CreatedOn
		it.UpdatedOn = &meta[i].UpdatedOn

		res.Nodes = append(res.Nodes, &ProjectNode{Position: it.Id, Node: it})
	}
	res.PageInfo.HasPrevious = page.HasPrevious
	res.PageInfo.HasNext = page.HasNext
	if len(list) > 0 {
		res.PageInfo.StartCursor = list[0].Id
		res.PageInfo.EndCursor = list[len(list)-1].Id
	}

	return res, nil

}

func (s *ProjectsServiceProjectServerCrud) GetPagedCondition(ctx context.Context, first uint32, after string, last uint32, before string) (*CursorBasedPagination, error) {

	page := &CursorBasedPagination{}
	flag := false

	if first != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(after),
			Limit:    int(first),
			UpOrDown: false,
		}

	} else if last != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(before),
			Limit:    int(last),
			UpOrDown: true,
		}
	}
	if !flag {
		return nil, status.Error(codes.InvalidArgument, "either after-first or before-last should be set in request")
	}

	return page, nil
}

func (s *ProjectsServiceProjectServerCrud) DeleteProject(ctx context.Context, in *DeleteProjectRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteProject(ctx, ProjectIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *ProjectsServiceProjectServerCrud) BatchGetProject(ctx context.Context, in *BatchGetProjectRequest) (*BatchGetProjectResponse, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.BatchGetProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	getIds := make([]string, 0, len(in.Ids))
	for _, id := range in.Ids {
		getIds = append(getIds, idutil.GetId(id))
	}

	mask := s.GetViewMask(in.ViewMask)

	meta := MetaInfoForList{}
	list, err := s.store.ListProjects(ctx, mask, ProjectIdIn{Id: getIds}, &meta)
	if err != nil {
		return nil, err
	}

	resultMap := make(map[string]*Project, 0)
	for i, it := range list {
		_ = i

		it.CreatedBy = meta[i].CreatedBy
		it.UpdatedBy = meta[i].UpdatedBy

		it.CreatedOn = &meta[i].CreatedOn
		it.UpdatedOn = &meta[i].UpdatedOn

		resultMap[it.Id] = it
	}

	isGrpc := userinfo.IsGrpcCall(ctx)

	result := make([]*Project, 0, len(in.Ids))
	for _, id := range in.Ids {
		if resultMap[id] == nil && isGrpc {
			result = append(result, &Project{})
			continue
		}
		result = append(result, resultMap[id])
	}

	return &BatchGetProjectResponse{Project: result}, nil
}

func (s *ProjectsServiceProjectServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
