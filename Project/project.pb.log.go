package project

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	activitLog "go.saastack.io/activity-log"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/idutil"
	"go.saastack.io/project/pb"
	"strings"
)

type logsProjectsServer struct {
	pb.ProjectsServer
	actLogCli activityLogPb.ActivityLogsClient
	str pb.ProjectStore
}

func NewLogsProjectsServer(a activityLogPb.ActivityLogsClient, s pb.ProjectsServer, str pb.ProjectStore) pb.ProjectsServer {

	srv := &logsProjectsServer{s, a, str}
	return srv
}

func (s *logsProjectsServer) CreateProject(ctx context.Context, in *pb.CreateProjectRequest) (*pb.Project, error) {

	res, err := s.ProjectsServer.CreateProject(ctx, in)
	if err != nil {
		return nil, err
	}

	project := &pb.Project{}
	if err = project.Update(res, pb.ProjectFields(
		pb.Project_Title,
	)); err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          in.GetParent(),
		Data:            project,
		EventName:       ".saastack.project.v1.Projects.CreateProject",
		ActivityId:      res.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) GetProject(ctx context.Context, in *pb.GetProjectRequest) (*pb.Project, error) {

	res, err := s.ProjectsServer.GetProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) DeleteProject(ctx context.Context, in *pb.DeleteProjectRequest) (*empty.Empty, error) {

	project, err := s.str.GetProject(ctx, []string{string(pb.Project_Title)}, pb.ProjectIdEq{Id: in.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.DeleteProject(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.Id),
		Data:            project,
		EventName:        ".saastack.project.v1.Projects.DeleteProject",
		ActivityId:      in.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) UpdateProject(ctx context.Context, in *pb.UpdateProjectRequest) (*pb.Project, error) {

	project, err := s.str.GetProject(ctx, []string{string(pb.Project_Title)}, pb.ProjectIdEq{Id: in.Project.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.UpdateProject(ctx, in)
	if err != nil {
		return nil, err
	}

	masks := pb.ProjectObjectCompare(project, res, "")
	objMask := []string{}
	for _, oldMask := range masks {
		if strings.Contains(oldMask, "address") || strings.Contains(oldMask, "gallery") {
			continue
		}
		objMask = append(objMask, oldMask)
	}

	newProject := &pb.Project{}
	if err = newProject.Update(res, objMask); err != nil {
		return nil, err
	}
	oldProject := &pb.Project{}
	if err = oldProject.Update(project, objMask); err != nil {
		return nil, err
	}


	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.Project.Id),
		Data:            map[string]interface{} {

			"old" : oldProject,
			"new" : newProject,
		},
		EventName:       ".saastack.project.v1.Projects.UpdateProject",
		ActivityId:      in.Project.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) ListProject(ctx context.Context, in *pb.ListProjectRequest) (*pb.ListProjectResponse, error) {

	res, err := s.ProjectsServer.ListProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) BatchGetProject(ctx context.Context, in *pb.BatchGetProjectRequest) (*pb.BatchGetProjectResponse, error) {

	res, err := s.ProjectsServer.BatchGetProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}
