package project

import (
	"context"
	"go.saastack.io/chaku/errors"
	company "go.saastack.io/company/pb"
	location "go.saastack.io/location/pb"
	"go.saastack.io/idutil"
	"google.golang.org/genproto/protobuf/field_mask"
	"go.saastack.io/project/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type projectsServer struct {
	projectStore pb.ProjectStore
	projectBLoC  pb.ProjectsServiceProjectServerBLoC
	*pb.ProjectsServiceProjectServerCrud
	parentCli pb.ParentServiceClient
}

func NewProjectsServer(

	projectSt pb.ProjectStore,
	parentCli pb.ParentServiceClient,

) pb.ProjectsServer {
	r := &projectsServer{

		projectStore: projectSt,
		parentCli: parentCli,
	}

	projectSC := pb.NewProjectsServiceProjectServerCrud(projectSt, r)
	r.ProjectsServiceProjectServerCrud = projectSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *projectsServer) CreateProjectBLoC(ctx context.Context, in *pb.CreateProjectRequest) error {

	if _, err := s.parentCli.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()}); err != nil {
		return err
	}

	return nil
}

func (s *projectsServer) GetProjectBLoC(ctx context.Context, in *pb.GetProjectRequest) error {

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}

func (s *projectsServer) UpdateProjectBLoC(ctx context.Context, in *pb.UpdateProjectRequest) error {

	//Validate view mask
	for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}

		if m == string(pb.Project_Id) {
			return status.Error(codes.InvalidArgument, "project id cannot be updated")
		}
	}

	return nil
}

func (s *projectsServer) DeleteProjectBLoC(ctx context.Context, in *pb.DeleteProjectRequest) error {
	return nil
}

func (s *projectsServer) BatchGetProjectBLoC(ctx context.Context, in *pb.BatchGetProjectRequest) error {

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}

func (s *projectsServer) ListProjectBLoC(ctx context.Context, in *pb.ListProjectRequest) (pb.ProjectCondition, error) {

	if in.GetFirst() == 0 && in.GetLast() == 0 {
		return nil, status.Error(codes.InvalidArgument, "either after-first or before-last should be set in request")
	}

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	// TODO ProjectFullParent compares the whole parent and ProjectParent compares only the id
	// TODO grp/cmp/loc all three needed to be there and in ProjectParent anyone will do
	return pb.ProjectFullParentEq{Parent: in.GetParent()}, nil
}

// These functions are not implemented by CRUDGen, needed to be implemented

// Parent Service

type parentServiceServer struct {
	locCli     location.LocationsClient
	companyCli company.CompaniesClient
}

// NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(lCli location.LocationsClient, companyCli company.CompaniesClient) pb.ParentServiceServer {
	return &parentServiceServer{
		locCli:     lCli,
		companyCli: companyCli,
	}
}

// ValidateParent ...
func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {

	prefix := idutil.GetPrefix(idutil.GetId(in.GetId()))

	switch prefix {
	case (&company.Company{}).GetPrefix():
		if _, err := s.companyCli.GetCompany(ctx, &company.GetCompanyRequest{
			Id:       in.GetId(),
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	case (&location.Location{}).GetPrefix():
		if _, err := s.locCli.GetLocation(ctx, &location.GetLocationRequest{
			Id:       in.Id,
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	default:
		return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
	}

	return &pb.ValidateParentResponse{Valid: true}, nil
}
