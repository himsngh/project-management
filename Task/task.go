package task

import (
	"context"
	"database/sql"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	sqlpx "github.com/srikrsna/sqlx/proto"
	"go.saastack.io/chaku/errors"
	company "go.saastack.io/company/pb"
	employeepb "go.saastack.io/employee/pb"
	"go.saastack.io/idutil"
	location "go.saastack.io/location/pb"
	project "go.saastack.io/project/pb"
	"go.saastack.io/protos/types"
	"go.saastack.io/task/pb"
	"go.saastack.io/userinfo"
	"go.uber.org/zap"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)


type tasksServer struct {
	taskStore pb.TaskStore
	taskBLoC  pb.TasksServiceTaskServerBLoC
	*pb.TasksServiceTaskServerCrud
	parentServer  pb.ParentServiceClient
	empCli employeepb.EmployeesClient
	projectCli project.ProjectsClient
	db *sql.DB
}

func NewTasksServer(

	taskSt pb.TaskStore,
	pSr pb.ParentServiceClient,
	empCli employeepb.EmployeesClient,
	projectCli project.ProjectsClient,
	db *sql.DB,

) pb.TasksServer {
	r := &tasksServer{
		taskStore: taskSt,
		parentServer:  pSr,
		empCli: empCli,
		projectCli: projectCli,
		db: db,
	}

	taskSC := pb.NewTasksServiceTaskServerCrud(taskSt, r)
	r.TasksServiceTaskServerCrud = taskSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *tasksServer) CreateTaskBLoC(ctx context.Context, in *pb.CreateTaskRequest) error {

	// Validate Parent
	prefix := idutil.GetPrefix(idutil.GetId(in.GetParent()))

	switch prefix {
	case (&project.Project{}).GetPrefix():
		if _, err := s.projectCli.GetProject(ctx, &project.GetProjectRequest{
			Id:       in.GetParent(),
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	default:
		return status.Error(codes.FailedPrecondition, "Invalid Parent")
	}

	// the user who created the task
	user := userinfo.FromContext(ctx)
	in.Task.Assignee = &pb.Assignee{
		Id:        user.Id,
		Email:     user.Email,
	}

	// Employee assigned id verification
	if in.GetTask().GetEmployeesId() != nil {
		err := s.ValidateEmployees(ctx,in.GetTask().GetEmployeesId()...)
		if err != nil {
			return  err
		}
	}
	// project information
	p , _ := s.projectCli.GetProject(ctx, &project.GetProjectRequest{
		Id:       in.GetParent(),
		ViewMask: &field_mask.FieldMask{Paths: []string{"title"}},
	})

	in.Task.ProjectName = p.GetTitle()
	in.Task.ProjectId = in.GetParent()

	return nil
}

func (s *tasksServer) GetTaskBLoC(ctx context.Context, in *pb.GetTaskRequest) error {

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil
}

func (s *tasksServer) UpdateTaskBLoC(ctx context.Context, in *pb.UpdateTaskRequest) error {

	for _, m := range in.UpdateMask.GetPaths() {

		if !pb.ValidTaskFieldMask(m) {
			return errors.ErrInvalidField
		}

		if m == string(pb.Task_Id) || m == "project_id" || m == "project_name" {
			return status.Error(codes.InvalidArgument, "can not update field " + m + " of the task")
		}

		// employee id verification
		if m == string(pb.Task_EmployeesId) {
			if in.GetTask().GetEmployeesId() != nil {

				if err := s.ValidateEmployees(ctx,in.GetTask().GetEmployeesId()...); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (s *tasksServer) DeleteTaskBLoC(ctx context.Context, in *pb.DeleteTaskRequest) error {
	return nil
}

func (s *tasksServer) ListTaskBLoC(ctx context.Context, in *pb.ListTaskRequest) (pb.TaskCondition, error) {

	if err := in.Validate(); err != nil {
		return nil, err
	}

	// Validate Parent
	_, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()})
	if err != nil {
		return nil, err
	}

	if in.GetFirst() == 0 && in.GetLast() == 0 {
		return nil, status.Error(codes.InvalidArgument, "either after-first or before-last should be set in request")
	}

	for _, m := range in.ViewMask.GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	// parent like
	return pb.TaskFullParentLike{Parent: in.GetParent() + "%"}, nil
}

// These functions are not implemented by CRUDGen, needed to be implemented


func (s *tasksServer) UpdateTaskStatus(ctx context.Context, in *pb.UpdateTaskStatusRequest) (*pb.Task, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	oldTask, err := s.taskStore.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	oldTask.Status = in.Status

	if in.GetStatus() == pb.Status_COMPLETED {
		oldTask.CompletedOn = ptypes.TimestampNow()
	}

	if err := s.taskStore.UpdateTask(ctx, oldTask, []string{"status","completed_on"}, pb.TaskIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	meta := pb.MetaInfo{}
	updatedTask, err := s.taskStore.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()}, &meta)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	updatedTask.CreatedOn = &meta.CreatedOn
	updatedTask.UpdatedOn = &meta.UpdatedOn
	updatedTask.UpdatedBy = meta.UpdatedBy
	updatedTask.CreatedBy = meta.CreatedBy


	return updatedTask, nil
}

func (s *tasksServer) AssignTask(ctx context.Context, in *pb.AssignTaskRequest) (*empty.Empty, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	task , err := s.taskStore.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	// Validating Employees assigning to the task
	if in.GetEmployeeId() != nil {
		err = s.ValidateEmployees(ctx,in.GetEmployeeId()...)
		if err != nil {
			return nil, err
		}
	}

	task.EmployeesId = append(task.EmployeesId, in.GetEmployeeId()...)

	if err := s.taskStore.UpdateTask(ctx, task, []string{"employees_id"}, pb.TaskIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

// For ListTaskByStatus, ListTaskByDate, ListTaskByTitle and TaskReport
// If Parent passed is at project level then it will return the data for all the tasks from that particular project
// If Parent passed is at location/company level then it will return the data for all the tasks from all projects

func (s *tasksServer) ListTaskByStatus(ctx context.Context, in *pb.ListTaskByStatusRequest) (*pb.ListTaskResponse, error) {
	
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Validate Parent
	_, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()})
	if err != nil {
		return nil,err
	}

	if in.GetViewMask() == nil || in.ViewMask.GetPaths() == nil {
		in.GetViewMask().Paths = []string{}
	}

	for _, m := range in.ViewMask.GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	meta := pb.MetaInfoForList{}
	list, err := s.taskStore.ListTasks(ctx, in.ViewMask.GetPaths(), pb.TaskAnd{pb.TaskFullParentLike{Parent: in.GetParent() + "%"},
				pb.TaskStatusEq{Status: in.GetStatus()}}, &meta)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	res := &pb.ListTaskResponse{
		PageInfo: &types.PageInfo{},
	}

	for i, it := range list {

		it.CreatedBy = meta[i].CreatedBy
		it.UpdatedBy = meta[i].UpdatedBy

		it.CreatedOn = &meta[i].CreatedOn
		it.UpdatedOn = &meta[i].UpdatedOn

		res.Nodes = append(res.Nodes, &pb.TaskNode{Position: it.Id, Node: it})
	}

	return res, nil
}

func (s *tasksServer) ListTaskByDate(ctx context.Context, in *pb.ListTaskByDateRequest) (*pb.ListTaskResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Validate Parent
	_, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()})
	if err != nil {
		return nil,err
	}


	if in.GetViewMask() == nil || in.ViewMask.GetPaths() == nil {
		in.GetViewMask().Paths = []string{}
	}

	for _, m := range in.ViewMask.GetPaths() {

		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	const query = `
		SELECT id FROM saastack_task_v1.task
  		WHERE parent like $1
			AND start_date >= $2
			AND due_date <= $3
`

	rows, err := s.db.QueryContext(ctx, query, in.GetParent() + "%",sqlpx.Timestamp(in.GetStartDate()), sqlpx.Timestamp(in.GetDueDate()))
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := rows.Close(); err != nil {
			ctxzap.Extract(ctx).Error("error closing query rows", zap.Error(err))
		}
	}()

	resp := &pb.ListTaskResponse{
		Nodes:    make([]*pb.TaskNode,0),
		PageInfo: &types.PageInfo{},
	}

	for rows.Next() {

		id := ""
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}

		task , err := s.taskStore.GetTask(ctx, in.GetViewMask().GetPaths(), pb.TaskIdEq{Id: id})
		if err != nil {
			continue
		}

		node := &pb.TaskNode{
			Node: task,
			Position: task.Id,
		}
		resp.Nodes = append(resp.Nodes, node)
	}

	return resp, nil
}

func (s *tasksServer) ListTaskByTitle(ctx context.Context, in *pb.ListTaskByTitleRequest) (*pb.ListTaskResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Validate Parent
	_, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()})
	if err != nil {
		return nil,err
	}

	if in.GetViewMask() == nil || in.ViewMask.GetPaths() == nil {
		in.GetViewMask().Paths = []string{}
	}

	for _, m := range in.ViewMask.GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	meta := pb.MetaInfoForList{}
	list, err := s.taskStore.ListTasks(ctx, in.GetViewMask().GetPaths(), pb.TaskAnd{pb.TaskFullParentLike{Parent: in.GetParent() + "%"},
		pb.TaskTitleLike{Title: "%" + in.GetTitle() + "%"}}, &meta)
	if err != nil {
		return nil, status.Error(codes.NotFound, err.Error())
	}



	res := &pb.ListTaskResponse{
		PageInfo: &types.PageInfo{},
	}

	for i, it := range list {

		it.CreatedBy = meta[i].CreatedBy
		it.UpdatedBy = meta[i].UpdatedBy

		it.CreatedOn = &meta[i].CreatedOn
		it.UpdatedOn = &meta[i].UpdatedOn

		res.Nodes = append(res.Nodes, &pb.TaskNode{Position: it.Id, Node: it})
	}

	return res, nil
}

// If Parent passed is at project level then it will return all the tasks completed in that particular project
// If Parent passed is at location level then it will return all the tasks completed under all the projects at that location
func (s *tasksServer) GetTaskReport(ctx context.Context, in *pb.GetTaskReportRequest) (*pb.GetTaskReportResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// TODO no need to validate parent here return empty rows
	_, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()})
	if err != nil {
		return nil, err
	}

	const query = `
		SELECT date_trunc('day', task.completed_on) AS date, count(id) 
		FROM saastack_task_v1.task
  		WHERE task.status = $1 AND parent like $4
			AND completed_on >= $2
			AND completed_on <= $3
		group by date
		ORDER BY date ASC
`

	rows, err := s.db.QueryContext(ctx, query, pb.Status_COMPLETED, sqlpx.Timestamp(in.GetStartDate()),
		sqlpx.Timestamp(in.GetDueDate()), "%" + in.Parent + "%")
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := rows.Close(); err != nil {
			ctxzap.Extract(ctx).Error("error closing query rows", zap.Error(err))
		}
	}()

	resp := &pb.GetTaskReportResponse{
		Data: make([]*pb.TaskReportResponse,0),
	}

	for rows.Next() {

		data := pb.TaskReportResponse{
			Date: &timestamp.Timestamp{},
		}
		if err := rows.Scan(sqlpx.Timestamp(data.Date), &data.Count); err != nil {
			return nil, err
		}

		resp.Data = append(resp.Data, &data)
	}

	return resp , nil
}

//func (s *tasksServer) GetTaskActivityDetails(ctx context.Context, in *pb.GetTaskActivityDetailsRequest) (*pb.GetTaskActivityResponse, error) {
//
//	if err := in.Validate(); err != nil {
//		return nil, err
//	}
//
//	const query = `
//			SELECT activity_log_data from saastack_activity_log_v2.activity_log
//			where activity_id = $1
//`
//	rows, err := s.db.QueryContext(ctx, query, in.GetId())
//	if err != nil {
//		return nil, err
//	}
//	defer func() {
//		if err := rows.Close(); err != nil {
//			ctxzap.Extract(ctx).Error("error closing query rows", zap.Error(err))
//		}
//	}()
//
//	resp := &pb.GetTaskActivityResponse{
//		Data: make([]string, 0),
//	}
//
//	for rows.Next() {
//
//		data := ""
//		if err := rows.Scan(&data); err != nil {
//			return nil, err
//		}
//
//		resp.Data = append(resp.Data, data)
//	}
//
//	return resp, nil
//}




func (s *tasksServer) ValidateEmployees(ctx context.Context, employeesId ...string) error {

		for _, id := range employeesId {
			if _, err := s.empCli.GetEmployee(ctx,&employeepb.GetEmployeeRequest{
				Id:       id,
				ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
			}); err != nil {
				return err
			}
		}
		return nil
}


// Parent Service

type parentServiceServer struct {
	companyCli company.CompaniesClient
	locationCli location.LocationsClient
	projectCli project.ProjectsClient
}

// NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(companyCli company.CompaniesClient, locationCli location.LocationsClient, pCli project.ProjectsClient) pb.ParentServiceServer {
	return &parentServiceServer{
		companyCli: companyCli,
		locationCli: locationCli,
		projectCli: pCli,
	}
}

// ValidateParent ...
func (s *parentServiceServer) ValidateParent (ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {

	prefix := idutil.GetPrefix(idutil.GetId(in.GetId()))

	switch prefix {
	case (&project.Project{}).GetPrefix():
		if _, err := s.projectCli.GetProject(ctx, &project.GetProjectRequest{
			Id:       in.GetId(),
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	case (&company.Company{}).GetPrefix():
		if _, err := s.companyCli.GetCompany(ctx, &company.GetCompanyRequest{
			Id:       in.GetId(),
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	case (&location.Location{}).GetPrefix():
		if _, err := s.locationCli.GetLocation(ctx, &location.GetLocationRequest{
			Id:       in.Id,
			ViewMask: &field_mask.FieldMask{Paths: []string{"id"}},
		}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}

	default:
		return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
	}

	return &pb.ValidateParentResponse{Valid: true}, nil
}
