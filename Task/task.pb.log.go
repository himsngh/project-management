package task

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	activitLog "go.saastack.io/activity-log"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/idutil"
	"go.saastack.io/task/pb"
)
//
type logsTasksServer struct {
	pb.TasksServer
	actLogCli activityLogPb.ActivityLogsClient
	str       pb.TaskStore
}

func NewLogsTasksServer(a activityLogPb.ActivityLogsClient, s pb.TasksServer, str pb.TaskStore) pb.TasksServer {

	srv := &logsTasksServer{s, a,str}
	return srv
}

func (s *logsTasksServer) CreateTask(ctx context.Context, in *pb.CreateTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.CreateTask(ctx, in)
	if err != nil {
		return nil, err
	}

	task := &pb.Task{}
	if err = task.Update(res, pb.TaskFields(
		pb.Task_Title,
		pb.Task_ProjectName,
	)); err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.GetParent()),
		Data:         	 task,
		EventName:       ".saastack.task.v1.Tasks.CreateTask",
		ActivityId:      res.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) GetTask(ctx context.Context, in *pb.GetTaskRequest) (*pb.Task, error) {

	res, err := s.TasksServer.GetTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) DeleteTask(ctx context.Context, in *pb.DeleteTaskRequest) (*empty.Empty, error) {

	task, err := s.str.GetTask(ctx, []string{string(pb.Task_Title)}, pb.TaskIdEq{Id: in.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.DeleteTask(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(idutil.GetParent(in.GetId())),
		Data:            task,
		EventName:       ".saastack.task.v1.Tasks.DeleteTask",
		ActivityId:      in.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) UpdateTask(ctx context.Context, in *pb.UpdateTaskRequest) (*pb.Task, error) {

	task, err := s.str.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.Task.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.UpdateTask(ctx, in)
	if err != nil {
		return nil, err
	}

	masks := pb.TaskObjectCompare(task, res, "")
	objMask := []string{}
	for _, oldMask := range masks {
		objMask = append(objMask, oldMask)
	}

	newTask := &pb.Task{}
	if err = newTask.Update(res, objMask); err != nil {
		return nil, err
	}
	oldTask := &pb.Task{}
	if err = oldTask.Update(task, objMask); err != nil {
		return nil, err
	}


	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(idutil.GetParent(in.Task.GetId())),
		Data:            map[string]interface{} {

			"old_task" : oldTask,
			"new_task" : newTask,
		},
		EventName:       ".saastack.task.v1.Tasks.UpdateTask",
		ActivityId:      in.Task.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTask(ctx context.Context, in *pb.ListTaskRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTask(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTaskByTitle(ctx context.Context, in *pb.ListTaskByTitleRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTaskByTitle(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTaskByStatus(ctx context.Context, in *pb.ListTaskByStatusRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTaskByStatus(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) ListTaskByDate(ctx context.Context, in *pb.ListTaskByDateRequest) (*pb.ListTaskResponse, error) {

	res, err := s.TasksServer.ListTaskByDate(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) AssignTask(ctx context.Context, in *pb.AssignTaskRequest) (*empty.Empty, error) {

	task, err := s.str.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.AssignTask(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(idutil.GetParent(in.Id)),
		Data:            map[string]interface{} {
						"title" : task.Id,
						"employeesAssigned" : in.EmployeeId,
		},
		EventName:       ".saastack.task.v1.Tasks.AssignTask",
		ActivityId:      in.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsTasksServer) UpdateTaskStatus(ctx context.Context, in *pb.UpdateTaskStatusRequest) (*pb.Task, error) {


	task, err := s.str.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.TasksServer.UpdateTaskStatus(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activitLog.CreateActivityLogWrapper(ctx , activitLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(idutil.GetParent(in.Id)),
		Data:            map[string]string{
			"oldStatus" : task.GetStatus().String(),
			"newStatus" : res.GetStatus().String(),
		},
		EventName:       ".saastack.task.v1.Tasks.UpdateTaskStatus",
		ActivityId:      in.Id,
		ActivityLogType: 0,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil
}
