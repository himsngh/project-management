package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"task": {
		"id":                 "task",
		"title":              "task",
		"start_date":         "task",
		"due_date":           "task",
		"send_notification":  "task",
		"notification_type":  "task",
		"status":             "task",
		"high_priority_task": "task",
		"employees_id":       "task",
		"project_id":         "task",
		"project_name":       "task",
		"completed_on":       "task",
		"assignee":           "assignee",
	},
	"assignee": {
		"id":    "assignee",
		"email": "assignee",
	},
}

func (m *Task) PackageName() string {
	return "saastack_task_v1"
}

func (m *Task) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Task) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	case "assignee":
		if m.Assignee == nil {
			m.Assignee = &Assignee{}
		}
		return m.Assignee, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) ObjectName() string {
	return "task"
}

func (m *Task) Fields() []string {
	return []string{
		"id", "title", "start_date", "due_date", "send_notification", "notification_type", "status", "high_priority_task", "employees_id", "project_id", "project_name", "completed_on", "assignee",
	}
}

func (m *Task) IsObject(field string) bool {
	switch field {
	case "assignee":
		return true
	default:
		return false
	}
}

func (m *Task) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Task) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "start_date":
		return m.StartDate, nil
	case "due_date":
		return m.DueDate, nil
	case "send_notification":
		return m.SendNotification, nil
	case "notification_type":
		return m.NotificationType, nil
	case "status":
		return m.Status, nil
	case "high_priority_task":
		return m.HighPriorityTask, nil
	case "employees_id":
		return m.EmployeesId, nil
	case "project_id":
		return m.ProjectId, nil
	case "project_name":
		return m.ProjectName, nil
	case "completed_on":
		return m.CompletedOn, nil
	case "assignee":
		return m.Assignee, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "start_date":
		return &m.StartDate, nil
	case "due_date":
		return &m.DueDate, nil
	case "send_notification":
		return &m.SendNotification, nil
	case "notification_type":
		return &m.NotificationType, nil
	case "status":
		return &m.Status, nil
	case "high_priority_task":
		return &m.HighPriorityTask, nil
	case "employees_id":
		return &m.EmployeesId, nil
	case "project_id":
		return &m.ProjectId, nil
	case "project_name":
		return &m.ProjectName, nil
	case "completed_on":
		return &m.CompletedOn, nil
	case "assignee":
		return &m.Assignee, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "start_date":
		if m.StartDate == nil {
			m.StartDate = &timestamp.Timestamp{Seconds: -62135596800}
		}
		return nil
	case "due_date":
		if m.DueDate == nil {
			m.DueDate = &timestamp.Timestamp{Seconds: -62135596800}
		}
		return nil
	case "send_notification":
		return nil
	case "notification_type":
		if m.NotificationType == nil {
			m.NotificationType = &NotificationType{}
		}
		return nil
	case "status":
		return nil
	case "high_priority_task":
		return nil
	case "employees_id":
		if m.EmployeesId == nil {
			m.EmployeesId = make([]string, 0)
		}
		return nil
	case "project_id":
		return nil
	case "project_name":
		return nil
	case "completed_on":
		if m.CompletedOn == nil {
			m.CompletedOn = &timestamp.Timestamp{Seconds: -62135596800}
		}
		return nil
	case "assignee":
		if m.Assignee == nil {
			m.Assignee = &Assignee{}
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Task) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "start_date":
		return "timestamp"
	case "due_date":
		return "timestamp"
	case "send_notification":
		return "bool"
	case "notification_type":
		return "json"
	case "status":
		return "enum"
	case "high_priority_task":
		return "bool"
	case "employees_id":
		return "repeated"
	case "project_id":
		return "string"
	case "project_name":
		return "string"
	case "completed_on":
		return "timestamp"
	case "assignee":
		return "message"
	default:
		return ""
	}
}

func (_ *Task) GetEmptyObject() (m *Task) {
	m = &Task{}
	_ = m.New("assignee")
	m.Assignee.GetEmptyObject()
	return
}

func (m *Task) GetPrefix() string {
	return "tas"
}

func (m *Task) GetID() string {
	return m.Id
}

func (m *Task) SetID(id string) {
	m.Id = id
}

func (m *Task) IsRoot() bool {
	return true
}

func (m *Task) IsFlatObject(f string) bool {
	return false
}

func (m *Assignee) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Assignee) ObjectName() string {
	return "assignee"
}

func (m *Assignee) Fields() []string {
	return []string{
		"id", "email",
	}
}

func (m *Assignee) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Assignee) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Assignee) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "email":
		return m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Assignee) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "email":
		return &m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Assignee) New(field string) error {
	switch field {
	case "id":
		return nil
	case "email":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Assignee) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "email":
		return "string"
	default:
		return ""
	}
}

func (_ *Assignee) GetEmptyObject() (m *Assignee) {
	m = &Assignee{}
	return
}

func (m *Assignee) GetPrefix() string {
	return ""
}

func (m *Assignee) GetID() string {
	return m.Id
}

func (m *Assignee) SetID(id string) {
	m.Id = id
}

func (m *Assignee) IsRoot() bool {
	return false
}

func (m *Assignee) IsFlatObject(f string) bool {
	return false
}

func (m *Task) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	case "assignee":
		return 1
	}
	return 0
}

type TaskStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s TaskStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s TaskStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewTaskStore(d driver.Driver) TaskStore {
	return TaskStore{d: d, limitMultiplier: 1}
}

func NewPostgresTaskStore(db *x.DB, usr driver.IUserInfo) TaskStore {
	return TaskStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type TaskTx struct {
	TaskStore
}

func (s TaskStore) BeginTx(ctx context.Context) (*TaskTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &TaskTx{
		TaskStore: TaskStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *TaskTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *TaskTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s TaskStore) CreateTaskPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_task_v1;
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task( id text DEFAULT ''::text , title text DEFAULT ''::text , start_date timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone , due_date timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone , send_notification boolean DEFAULT false , notification_type jsonb DEFAULT '{}'::jsonb , status integer DEFAULT 0 , high_priority_task boolean DEFAULT false , employees_id text[] DEFAULT '{}'::text[] , project_id text DEFAULT ''::text , project_name text DEFAULT ''::text , completed_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_task_v1.assignee( id text DEFAULT ''::text , email text DEFAULT ''::text , p0id text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, p0id)); 
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s TaskStore) CreateTasks(ctx context.Context, list ...*Task) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
}

func (s TaskStore) DeleteTask(ctx context.Context, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
	}
	return s.d.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

func (s TaskStore) UpdateTask(ctx context.Context, req *Task, fields []string, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
	}
	return s.d.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
}

func (s TaskStore) UpdateTaskMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Task{}, &Task{}, list...)
}

func (s TaskStore) GetTask(ctx context.Context, fields []string, cond TaskCondition, opt ...getTasksOption) (*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listTasksOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListTasks(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TaskStore) ListTasks(ctx context.Context, fields []string, cond TaskCondition, opt ...listTasksOption) ([]*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetTaskCondition == nil {
					page.SetTaskCondition = defaultSetTaskCondition
				}
				cond = page.SetTaskCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Task, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Task{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperTask(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TaskStore) CountTasks(ctx context.Context, cond TaskCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

type getTasksOption interface {
	getOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptTasks() { // method of no significant use
}

type listTasksOption interface {
	listOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptTasks() {
}

func (OrderBy) listOptTasks() {
}

func (*CursorBasedPagination) listOptTasks() {
}

func defaultSetTaskCondition(upOrDown bool, cursor string, cond TaskCondition) TaskCondition {
	if upOrDown {
		if cursor != "" {
			return TaskAnd{cond, TaskIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return TaskAnd{cond, TaskIdGt{cursor}}
	}
	return cond
}

type TaskAnd []TaskCondition

func (p TaskAnd) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type TaskOr []TaskCondition

func (p TaskOr) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type TaskParentEq struct {
	Parent string
}

func (c TaskParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentEq struct {
	Parent string
}

func (c TaskFullParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotEq struct {
	Parent string
}

func (c TaskParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotEq struct {
	Parent string
}

func (c TaskFullParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentLike struct {
	Parent string
}

func (c TaskParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentLike struct {
	Parent string
}

func (c TaskFullParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentILike struct {
	Parent string
}

func (c TaskParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentILike struct {
	Parent string
}

func (c TaskFullParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentIn struct {
	Parent []string
}

func (c TaskParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentIn struct {
	Parent []string
}

func (c TaskFullParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotIn struct {
	Parent []string
}

func (c TaskParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotIn struct {
	Parent []string
}

func (c TaskFullParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdEq struct {
	Id string
}

func (c TaskIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleEq struct {
	Title string
}

func (c TaskTitleEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateEq struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateEq struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationEq struct {
	SendNotification bool
}

func (c TaskSendNotificationEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusEq struct {
	Status Status
}

func (c TaskStatusEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskEq struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdEq struct {
	EmployeesId []string
}

func (c TaskEmployeesIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdEq struct {
	ProjectId string
}

func (c TaskProjectIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameEq struct {
	ProjectName string
}

func (c TaskProjectNameEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnEq struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdEq struct {
	Id string
}

func (c TaskAssigneeIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailEq struct {
	Email string
}

func (c TaskAssigneeEmailEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotEq struct {
	Id string
}

func (c TaskIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotEq struct {
	Title string
}

func (c TaskTitleNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateNotEq struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateNotEq struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationNotEq struct {
	SendNotification bool
}

func (c TaskSendNotificationNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusNotEq struct {
	Status Status
}

func (c TaskStatusNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskNotEq struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdNotEq struct {
	EmployeesId []string
}

func (c TaskEmployeesIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdNotEq struct {
	ProjectId string
}

func (c TaskProjectIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameNotEq struct {
	ProjectName string
}

func (c TaskProjectNameNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnNotEq struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdNotEq struct {
	Id string
}

func (c TaskAssigneeIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailNotEq struct {
	Email string
}

func (c TaskAssigneeEmailNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGt struct {
	Id string
}

func (c TaskIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGt struct {
	Title string
}

func (c TaskTitleGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateGt struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateGt struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationGt struct {
	SendNotification bool
}

func (c TaskSendNotificationGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusGt struct {
	Status Status
}

func (c TaskStatusGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskGt struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdGt struct {
	EmployeesId []string
}

func (c TaskEmployeesIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdGt struct {
	ProjectId string
}

func (c TaskProjectIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameGt struct {
	ProjectName string
}

func (c TaskProjectNameGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnGt struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdGt struct {
	Id string
}

func (c TaskAssigneeIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailGt struct {
	Email string
}

func (c TaskAssigneeEmailGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLt struct {
	Id string
}

func (c TaskIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLt struct {
	Title string
}

func (c TaskTitleLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateLt struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateLt struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationLt struct {
	SendNotification bool
}

func (c TaskSendNotificationLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusLt struct {
	Status Status
}

func (c TaskStatusLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskLt struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdLt struct {
	EmployeesId []string
}

func (c TaskEmployeesIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdLt struct {
	ProjectId string
}

func (c TaskProjectIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameLt struct {
	ProjectName string
}

func (c TaskProjectNameLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnLt struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdLt struct {
	Id string
}

func (c TaskAssigneeIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailLt struct {
	Email string
}

func (c TaskAssigneeEmailLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGtOrEq struct {
	Id string
}

func (c TaskIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGtOrEq struct {
	Title string
}

func (c TaskTitleGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateGtOrEq struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateGtOrEq struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationGtOrEq struct {
	SendNotification bool
}

func (c TaskSendNotificationGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusGtOrEq struct {
	Status Status
}

func (c TaskStatusGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskGtOrEq struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdGtOrEq struct {
	EmployeesId []string
}

func (c TaskEmployeesIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdGtOrEq struct {
	ProjectId string
}

func (c TaskProjectIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameGtOrEq struct {
	ProjectName string
}

func (c TaskProjectNameGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnGtOrEq struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdGtOrEq struct {
	Id string
}

func (c TaskAssigneeIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailGtOrEq struct {
	Email string
}

func (c TaskAssigneeEmailGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLtOrEq struct {
	Id string
}

func (c TaskIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLtOrEq struct {
	Title string
}

func (c TaskTitleLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateLtOrEq struct {
	StartDate *timestamp.Timestamp
}

func (c TaskStartDateLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateLtOrEq struct {
	DueDate *timestamp.Timestamp
}

func (c TaskDueDateLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationLtOrEq struct {
	SendNotification bool
}

func (c TaskSendNotificationLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusLtOrEq struct {
	Status Status
}

func (c TaskStatusLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskLtOrEq struct {
	HighPriorityTask bool
}

func (c TaskHighPriorityTaskLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdLtOrEq struct {
	EmployeesId []string
}

func (c TaskEmployeesIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdLtOrEq struct {
	ProjectId string
}

func (c TaskProjectIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameLtOrEq struct {
	ProjectName string
}

func (c TaskProjectNameLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnLtOrEq struct {
	CompletedOn *timestamp.Timestamp
}

func (c TaskCompletedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdLtOrEq struct {
	Id string
}

func (c TaskAssigneeIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailLtOrEq struct {
	Email string
}

func (c TaskAssigneeEmailLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLike struct {
	Id string
}

func (c TaskIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLike struct {
	Title string
}

func (c TaskTitleLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdLike struct {
	EmployeesId []string
}

func (c TaskEmployeesIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdLike struct {
	ProjectId string
}

func (c TaskProjectIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameLike struct {
	ProjectName string
}

func (c TaskProjectNameLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdLike struct {
	Id string
}

func (c TaskAssigneeIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailLike struct {
	Email string
}

func (c TaskAssigneeEmailLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdILike struct {
	Id string
}

func (c TaskIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleILike struct {
	Title string
}

func (c TaskTitleILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdILike struct {
	EmployeesId []string
}

func (c TaskEmployeesIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdILike struct {
	ProjectId string
}

func (c TaskProjectIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameILike struct {
	ProjectName string
}

func (c TaskProjectNameILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdILike struct {
	Id string
}

func (c TaskAssigneeIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailILike struct {
	Email string
}

func (c TaskAssigneeEmailILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskEmployeesIdArrayOverlap struct {
	EmployeesId []string
}

func (c TaskEmployeesIdArrayOverlap) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ArrayOverlap{Key: "employees_id", Value: c.EmployeesId, Operator: d, Descriptor: &Task{}, FieldMask: "employees_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeleted struct {
	IsDeleted bool
}

func (c TaskDeleted) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeDeleted struct {
	IsDeleted bool
}

func (c TaskAssigneeDeleted) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByEq struct {
	By string
}

func (c TaskCreatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByNotEq struct {
	By string
}

func (c TaskCreatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGt struct {
	By string
}

func (c TaskCreatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLt struct {
	By string
}

func (c TaskCreatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGtOrEq struct {
	By string
}

func (c TaskCreatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLtOrEq struct {
	By string
}

func (c TaskCreatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLike struct {
	By string
}

func (c TaskCreatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByILike struct {
	By string
}

func (c TaskCreatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByEq struct {
	By string
}

func (c TaskUpdatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByNotEq struct {
	By string
}

func (c TaskUpdatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGt struct {
	By string
}

func (c TaskUpdatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLt struct {
	By string
}

func (c TaskUpdatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGtOrEq struct {
	By string
}

func (c TaskUpdatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLtOrEq struct {
	By string
}

func (c TaskUpdatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLike struct {
	By string
}

func (c TaskUpdatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByILike struct {
	By string
}

func (c TaskUpdatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByEq struct {
	By string
}

func (c TaskDeletedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByNotEq struct {
	By string
}

func (c TaskDeletedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGt struct {
	By string
}

func (c TaskDeletedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLt struct {
	By string
}

func (c TaskDeletedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGtOrEq struct {
	By string
}

func (c TaskDeletedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLtOrEq struct {
	By string
}

func (c TaskDeletedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLike struct {
	By string
}

func (c TaskDeletedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByILike struct {
	By string
}

func (c TaskDeletedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdIn struct {
	Id []string
}

func (c TaskIdIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleIn struct {
	Title []string
}

func (c TaskTitleIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateIn struct {
	StartDate []*timestamp.Timestamp
}

func (c TaskStartDateIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateIn struct {
	DueDate []*timestamp.Timestamp
}

func (c TaskDueDateIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationIn struct {
	SendNotification []bool
}

func (c TaskSendNotificationIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusIn struct {
	Status []Status
}

func (c TaskStatusIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskIn struct {
	HighPriorityTask []bool
}

func (c TaskHighPriorityTaskIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdIn struct {
	ProjectId []string
}

func (c TaskProjectIdIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameIn struct {
	ProjectName []string
}

func (c TaskProjectNameIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnIn struct {
	CompletedOn []*timestamp.Timestamp
}

func (c TaskCompletedOnIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdIn struct {
	Id []string
}

func (c TaskAssigneeIdIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailIn struct {
	Email []string
}

func (c TaskAssigneeEmailIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotIn struct {
	Id []string
}

func (c TaskIdNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotIn struct {
	Title []string
}

func (c TaskTitleNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStartDateNotIn struct {
	StartDate []*timestamp.Timestamp
}

func (c TaskStartDateNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "start_date", Value: c.StartDate, Operator: d, Descriptor: &Task{}, FieldMask: "start_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueDateNotIn struct {
	DueDate []*timestamp.Timestamp
}

func (c TaskDueDateNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "due_date", Value: c.DueDate, Operator: d, Descriptor: &Task{}, FieldMask: "due_date", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotificationNotIn struct {
	SendNotification []bool
}

func (c TaskSendNotificationNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "send_notification", Value: c.SendNotification, Operator: d, Descriptor: &Task{}, FieldMask: "send_notification", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusNotIn struct {
	Status []Status
}

func (c TaskStatusNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorityTaskNotIn struct {
	HighPriorityTask []bool
}

func (c TaskHighPriorityTaskNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "high_priority_task", Value: c.HighPriorityTask, Operator: d, Descriptor: &Task{}, FieldMask: "high_priority_task", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectIdNotIn struct {
	ProjectId []string
}

func (c TaskProjectIdNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "project_id", Value: c.ProjectId, Operator: d, Descriptor: &Task{}, FieldMask: "project_id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskProjectNameNotIn struct {
	ProjectName []string
}

func (c TaskProjectNameNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "project_name", Value: c.ProjectName, Operator: d, Descriptor: &Task{}, FieldMask: "project_name", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCompletedOnNotIn struct {
	CompletedOn []*timestamp.Timestamp
}

func (c TaskCompletedOnNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "completed_on", Value: c.CompletedOn, Operator: d, Descriptor: &Task{}, FieldMask: "completed_on", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIdNotIn struct {
	Id []string
}

func (c TaskAssigneeIdNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEmailNotIn struct {
	Email []string
}

func (c TaskAssigneeEmailNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "assignee.email", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

func (c TrueCondition) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type taskMapperObject struct {
	id               string
	title            string
	startDate        *timestamp.Timestamp
	dueDate          *timestamp.Timestamp
	sendNotification bool
	notificationType *NotificationType
	status           Status
	highPriorityTask bool
	employeesId      []string
	projectId        string
	projectName      string
	completedOn      *timestamp.Timestamp
	assignee         map[string]*taskAssigneeMapperObject
}

func (s *taskMapperObject) GetUniqueIdentifier() string {
	return s.id
}

type taskAssigneeMapperObject struct {
	id    string
	email string
}

func (s *taskAssigneeMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperTask(rows []*Task) []*Task {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedTaskMappers := map[string]*taskMapperObject{}

	for _, rw := range rows {

		tempTask := &taskMapperObject{}
		tempTaskAssignee := &taskAssigneeMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		if rw.Assignee == nil {
			rw.Assignee = rw.Assignee.GetEmptyObject()
		}
		tempTaskAssignee.id = rw.Assignee.Id
		tempTaskAssignee.email = rw.Assignee.Email

		tempTask.id = rw.Id
		tempTask.title = rw.Title
		tempTask.startDate = rw.StartDate
		tempTask.dueDate = rw.DueDate
		tempTask.sendNotification = rw.SendNotification
		tempTask.notificationType = rw.NotificationType
		tempTask.status = rw.Status
		tempTask.highPriorityTask = rw.HighPriorityTask
		tempTask.employeesId = rw.EmployeesId
		tempTask.projectId = rw.ProjectId
		tempTask.projectName = rw.ProjectName
		tempTask.completedOn = rw.CompletedOn
		tempTask.assignee = map[string]*taskAssigneeMapperObject{
			tempTaskAssignee.GetUniqueIdentifier(): tempTaskAssignee,
		}

		if combinedTaskMappers[tempTask.GetUniqueIdentifier()] == nil {
			combinedTaskMappers[tempTask.GetUniqueIdentifier()] = tempTask
		} else {

			taskMapper := combinedTaskMappers[tempTask.GetUniqueIdentifier()]

			if taskMapper.assignee[tempTaskAssignee.GetUniqueIdentifier()] == nil {
				taskMapper.assignee[tempTaskAssignee.GetUniqueIdentifier()] = tempTaskAssignee
			}
			combinedTaskMappers[tempTask.GetUniqueIdentifier()] = taskMapper
		}

	}

	combinedTasks := make(map[string]*Task, 0)

	for _, task := range combinedTaskMappers {
		tempTask := &Task{}
		tempTask.Id = task.id
		tempTask.Title = task.title
		tempTask.StartDate = task.startDate
		tempTask.DueDate = task.dueDate
		tempTask.SendNotification = task.sendNotification
		tempTask.NotificationType = task.notificationType
		tempTask.Status = task.status
		tempTask.HighPriorityTask = task.highPriorityTask
		tempTask.EmployeesId = task.employeesId
		tempTask.ProjectId = task.projectId
		tempTask.ProjectName = task.projectName
		tempTask.CompletedOn = task.completedOn

		combinedTaskAssignees := []*Assignee{}

		for _, taskAssignee := range task.assignee {
			tempTaskAssignee := &Assignee{}
			tempTaskAssignee.Id = taskAssignee.id
			tempTaskAssignee.Email = taskAssignee.email

			if tempTaskAssignee.Id == "" {
				continue
			}

			combinedTaskAssignees = append(combinedTaskAssignees, tempTaskAssignee)

		}
		if len(combinedTaskAssignees) == 0 {
			tempTask.Assignee = nil
		} else {
			tempTask.Assignee = combinedTaskAssignees[0]

		}

		if tempTask.Id == "" {
			continue
		}

		combinedTasks[tempTask.Id] = tempTask

	}
	list := make([]*Task, 0, len(combinedTasks))
	for _, i := range ids {
		list = append(list, combinedTasks[i])
	}
	return list
}

func (s TaskStore) CreateAssignees(ctx context.Context, objFieldMask string, pid []string, list ...*Assignee) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Assignee{}, &Task{}, objFieldMask, pid)
	}
	return s.d.Insert(ctx, vv, &Assignee{}, &Task{}, objFieldMask, pid)
}

func (s TaskStore) DeleteAssignee(ctx context.Context, cond AssigneeCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.assigneeCondToDriverTaskCond(s.d), &Assignee{}, &Task{})
	}
	return s.d.Delete(ctx, cond.assigneeCondToDriverTaskCond(s.d), &Assignee{}, &Task{})
}

func (s TaskStore) UpdateAssignee(ctx context.Context, req *Assignee, fields []string, cond AssigneeCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.assigneeCondToDriverTaskCond(s.d), req, &Task{}, fields...)
	}
	return s.d.Update(ctx, cond.assigneeCondToDriverTaskCond(s.d), req, &Task{}, fields...)
}

func (s TaskStore) UpdateAssigneeMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Assignee{}, &Task{}, list...)
}

func (s TaskStore) GetAssignee(ctx context.Context, fields []string, cond AssigneeCondition, opt ...getAssigneesTaskOption) (*Assignee, error) {
	if len(fields) == 0 {
		fields = (&Assignee{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listAssigneesTaskOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListAssignees(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TaskStore) ListAssignees(ctx context.Context, fields []string, cond AssigneeCondition, opt ...listAssigneesTaskOption) ([]*Assignee, error) {
	if len(fields) == 0 {
		fields = (&Assignee{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetAssigneeCondition == nil {
					page.SetAssigneeCondition = defaultSetAssigneeCondition
				}
				cond = page.SetAssigneeCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.assigneeCondToDriverTaskCond(s.d), &Assignee{}, &Task{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.assigneeCondToDriverTaskCond(s.d), &Assignee{}, &Task{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Assignee, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Assignee{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperAssignee(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TaskStore) CountAssignees(ctx context.Context, cond AssigneeCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.assigneeCondToDriverTaskCond(s.d), &Assignee{}, &Task{})
}

type getAssigneesTaskOption interface {
	getOptAssigneesTask() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptAssigneesTask() { // method of no significant use
}

type listAssigneesTaskOption interface {
	listOptAssigneesTask() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptAssigneesTask() {
}

func (OrderBy) listOptAssigneesTask() {
}

func (*CursorBasedPagination) listOptAssigneesTask() {
}

func defaultSetAssigneeCondition(upOrDown bool, cursor string, cond AssigneeCondition) AssigneeCondition {
	if upOrDown {
		if cursor != "" {
			return AssigneeAnd{cond, AssigneeIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return AssigneeAnd{cond, AssigneeIdGt{cursor}}
	}
	return cond
}

type AssigneeAnd []AssigneeCondition

func (p AssigneeAnd) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.assigneeCondToDriverTaskCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AssigneeOr []AssigneeCondition

func (p AssigneeOr) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.assigneeCondToDriverTaskCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type AssigneeParentEq struct {
	Parent string
}

func (c AssigneeParentEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeParentNotEq struct {
	Parent string
}

func (c AssigneeParentNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeParentLike struct {
	Parent string
}

func (c AssigneeParentLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeParentILike struct {
	Parent string
}

func (c AssigneeParentILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeParentIn struct {
	Parent []string
}

func (c AssigneeParentIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeParentNotIn struct {
	Parent []string
}

func (c AssigneeParentNotIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdEq struct {
	Id string
}

func (c AssigneeIdEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailEq struct {
	Email string
}

func (c AssigneeEmailEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdNotEq struct {
	Id string
}

func (c AssigneeIdNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailNotEq struct {
	Email string
}

func (c AssigneeEmailNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdGt struct {
	Id string
}

func (c AssigneeIdGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailGt struct {
	Email string
}

func (c AssigneeEmailGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdLt struct {
	Id string
}

func (c AssigneeIdLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailLt struct {
	Email string
}

func (c AssigneeEmailLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdGtOrEq struct {
	Id string
}

func (c AssigneeIdGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailGtOrEq struct {
	Email string
}

func (c AssigneeEmailGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdLtOrEq struct {
	Id string
}

func (c AssigneeIdLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailLtOrEq struct {
	Email string
}

func (c AssigneeEmailLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdLike struct {
	Id string
}

func (c AssigneeIdLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailLike struct {
	Email string
}

func (c AssigneeEmailLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdILike struct {
	Id string
}

func (c AssigneeIdILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailILike struct {
	Email string
}

func (c AssigneeEmailILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeleted struct {
	IsDeleted bool
}

func (c AssigneeDeleted) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByEq struct {
	By string
}

func (c AssigneeCreatedByEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByNotEq struct {
	By string
}

func (c AssigneeCreatedByNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByGt struct {
	By string
}

func (c AssigneeCreatedByGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByLt struct {
	By string
}

func (c AssigneeCreatedByLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByGtOrEq struct {
	By string
}

func (c AssigneeCreatedByGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByLtOrEq struct {
	By string
}

func (c AssigneeCreatedByLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeCreatedOnLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByLike struct {
	By string
}

func (c AssigneeCreatedByLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeCreatedByILike struct {
	By string
}

func (c AssigneeCreatedByILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByEq struct {
	By string
}

func (c AssigneeUpdatedByEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByNotEq struct {
	By string
}

func (c AssigneeUpdatedByNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByGt struct {
	By string
}

func (c AssigneeUpdatedByGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByLt struct {
	By string
}

func (c AssigneeUpdatedByLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByGtOrEq struct {
	By string
}

func (c AssigneeUpdatedByGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByLtOrEq struct {
	By string
}

func (c AssigneeUpdatedByLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeUpdatedOnLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByLike struct {
	By string
}

func (c AssigneeUpdatedByLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeUpdatedByILike struct {
	By string
}

func (c AssigneeUpdatedByILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByEq struct {
	By string
}

func (c AssigneeDeletedByEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByNotEq struct {
	By string
}

func (c AssigneeDeletedByNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnNotEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByGt struct {
	By string
}

func (c AssigneeDeletedByGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnGt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByLt struct {
	By string
}

func (c AssigneeDeletedByLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnLt) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByGtOrEq struct {
	By string
}

func (c AssigneeDeletedByGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnGtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByLtOrEq struct {
	By string
}

func (c AssigneeDeletedByLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AssigneeDeletedOnLtOrEq) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByLike struct {
	By string
}

func (c AssigneeDeletedByLike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeDeletedByILike struct {
	By string
}

func (c AssigneeDeletedByILike) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Assignee{}, RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdIn struct {
	Id []string
}

func (c AssigneeIdIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailIn struct {
	Email []string
}

func (c AssigneeEmailIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeIdNotIn struct {
	Id []string
}

func (c AssigneeIdNotIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Assignee{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

type AssigneeEmailNotIn struct {
	Email []string
}

func (c AssigneeEmailNotIn) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Assignee{}, FieldMask: "email", RootDescriptor: &Task{}, CurrentDescriptor: &Assignee{}}
}

func (c TrueCondition) assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type assigneeMapperObject struct {
	id    string
	email string
}

func (s *assigneeMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperAssignee(rows []*Assignee) []*Assignee {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedAssigneeMappers := map[string]*assigneeMapperObject{}

	for _, rw := range rows {

		tempAssignee := &assigneeMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempAssignee.id = rw.Id
		tempAssignee.email = rw.Email

		if combinedAssigneeMappers[tempAssignee.GetUniqueIdentifier()] == nil {
			combinedAssigneeMappers[tempAssignee.GetUniqueIdentifier()] = tempAssignee
		}
	}

	combinedAssignees := make(map[string]*Assignee, 0)

	for _, assignee := range combinedAssigneeMappers {
		tempAssignee := &Assignee{}
		tempAssignee.Id = assignee.id
		tempAssignee.Email = assignee.email

		if tempAssignee.Id == "" {
			continue
		}

		combinedAssignees[tempAssignee.Id] = tempAssignee

	}
	list := make([]*Assignee, 0, len(combinedAssignees))
	for _, i := range ids {
		list = append(list, combinedAssignees[i])
	}
	return list
}

func (m *Task) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type TaskCondition interface {
	taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner
}
type AssigneeCondition interface {
	assigneeCondToDriverTaskCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetTaskCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetTaskCondition func(upOrDown bool, cursor string, cond TaskCondition) TaskCondition
	// SetAssigneeCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetAssigneeCondition func(upOrDown bool, cursor string, cond AssigneeCondition) AssigneeCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
