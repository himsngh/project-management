syntax = "proto3";

package saastack.task.v1;

option go_package = "pb";

import "annotations/annotations.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/field_mask.proto";
import "google/protobuf/timestamp.proto";
import "validate/validate.proto";
import "validate/chaku.proto";
import "types/types.proto";
import "pehredaar/pehredaar.proto";
import "eventspush/push.proto";
import "schema/schema.proto";
import "logging/log.proto";
import "crudgen/crudgen.proto";

service Tasks {

    // CreateTask creates new task.
    rpc CreateTask (CreateTaskRequest) returns (Task) {
        option (google.api.http) = {
            post: "/v1/tasks"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "parent"
            allow_parent: true
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "createTask"
        };
    }

    // GetTask returns the task by its unique id.
    rpc GetTask (GetTaskRequest) returns (Task) {
        option (google.api.http) = {
            get: "/v1/tasks/{id}"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (graphql.schema) = {
            query : "task"
        };

        option (logging.skip_log) = true;
    }

    // DeleteTask will delete the task from the system by Id.
    // This will be a soft delete from the system
    rpc DeleteTask (DeleteTaskRequest) returns (google.protobuf.Empty) {
        option (google.api.http) = {
            delete: "/v1/tasks/{id}"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "deleteTask"
        };

    }

    // UpdateTask will update the task identified by its task id.
    // Update Task uses Field Mask to update specific properties of task object
    rpc UpdateTask (UpdateTaskRequest) returns (Task) {
        option (google.api.http) = {
            put: "/v1/tasks/{task.id}"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "task.id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "updateTask"
        };
    }

    // ListTask lists all the Task(s)
    rpc ListTask (ListTaskRequest) returns (ListTaskResponse) {
        option (google.api.http) = {
            get : "/v1/tasks"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasks"
        };

        option (logging.skip_log) = true;
    }

    // UpdateTaskStatus will update the status of the task
    rpc UpdateTaskStatus (UpdateTaskStatusRequest) returns (Task) {
        option (google.api.http) = {
            put: "/v1/tasks/status/{id}"
            body: "*"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "updateTaskStatus"
        };
    }

    // AssignTask will assign employee(s) to the task
    rpc AssignTask (AssignTaskRequest) returns (.google.protobuf.Empty) {
        option (google.api.http) = {
            post : "/v1/tasks/assignTask"
            body : "*"
        };
        option (pehredaar.paths) = {
            resource: "id"
        };
        option (eventspush.event) = {
            push: true
        };
        option (graphql.schema) = {
            mutation : "assignTask"
        };
    }

    // List Task by title according to the parent
    // if the parent is project list task by title inside the project
    // if the parent is location/company then list all the tasks from all the projects inside that location/company
    rpc ListTaskByTitle (ListTaskByTitleRequest) returns (ListTaskResponse) {
        option (google.api.http) = {
            get: "/v1/tasks/title"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasksByTitle"
        };

        option (logging.skip_log) = true;
    }

    // List task by status (similar to List Task by Title)
    rpc ListTaskByStatus (ListTaskByStatusRequest) returns (ListTaskResponse) {
        option (google.api.http) = {
            get: "/v1/tasks/status"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasksByStatus"
        };

        option (logging.skip_log) = true;
    }

    // List task by date (similar to list task by title)
    rpc ListTaskByDate (ListTaskByDateRequest) returns (ListTaskResponse) {
        option (google.api.http) = {
            get: "/v1/tasks/date"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasksByDate"
        };

        option (logging.skip_log) = true;
    }

    // Get the report for tasks completed in a particular range of time.
    // if the parent is project then get the report for the project
    // if the parent is company/location then get the report for that.
    rpc GetTaskReport (GetTaskReportRequest) returns (GetTaskReportResponse) {
        option (google.api.http) = {
            get: "/v1/tasks/report"
        };
        option (pehredaar.paths) = {
            resource: "parent"
        };
        option (graphql.schema) = {
            query : "tasksReport"
        };

        option (logging.skip_log) = true;
    }

    // activity-log data  of a particular task
//    rpc GetTaskActivityDetails(GetTaskActivityDetailsRequest) returns (GetTaskActivityResponse) {
//        option (google.api.http) = {
//            get: "/v1/tasks/activity"
//        };
//        option (pehredaar.paths) = {
//            resource: "id"
//        };
//        option (graphql.schema) = {
//            query : "tasksActivity"
//        };
//
//        option (logging.skip_log) = true;
//    }

}

message Task {

    option (chaku.root) = true;
    option (chaku.prefix) = 'tas';
    option (crudgen.meta_details) = true;

    string id = 1;

    // title of the task
    string title = 2 [(validate.rules).string = {min_len: 1 max_len: 100}];
    // start date and end date of the task
    .google.protobuf.Timestamp start_date = 3 [(validate.rules).timestamp.required = true];
    .google.protobuf.Timestamp due_date = 4 [(validate.rules).timestamp.required = true];
    // send notification or not
    bool send_notification = 5;
    // if send notification then type of notification
    NotificationType notification_type = 6 [(chaku.constraints).json = true];
    // task is assigned by (the owner of the task)
    Assignee assignee = 7 [(graphql.input_skip) = true];
    // Status of the task
    Status status = 8 [(validate.rules).enum.defined_only = true];
    // priority of the task
    bool high_priority_task = 9;
    // ids of employees assigned to this task
    repeated string employees_id = 10; // TODO naming convention ids.
    // project (id,name) of the task // TODO No need to have these fields try to make it as abstract as possible
    string project_id = 11;
    string project_name = 12;

    // created by and created on
    string created_by = 13 [(graphql.input_skip) = true];
    google.protobuf.Timestamp created_on = 14 [(graphql.input_skip) = true];
    // updated by and updated on
    string updated_by = 15 [(graphql.input_skip) = true];
    google.protobuf.Timestamp updated_on = 16 [(graphql.input_skip) = true];

    // task completed on
    google.protobuf.Timestamp completed_on = 17 [(graphql.input_skip) = true];
}

enum Status {
    UNSPECIFIED = 0;
    ONGOING = 1;
    COMPLETED = 2;
    UPCOMING = 3;
}

message NotificationType {
    bool message = 1;
    bool email = 2;
}

message Assignee {
    // id of the assignee
    string id = 1;
    // email of the assignee
    string email = 4;
}

message CreateTaskRequest {
    string parent = 1 [(validate.rules).string.min_len = 3];
    Task task = 2 [(validate.rules).message.required = true];
}

message GetTaskRequest {
    string id = 1 [(validate.rules).string.min_len = 3];
    google.protobuf.FieldMask view_mask = 2;
}

message DeleteTaskRequest {
    string id = 1 [(validate.rules).string.min_len = 3];
}

message UpdateTaskRequest {
    Task task = 1 [(validate.rules).message.required = true];
    google.protobuf.FieldMask update_mask = 2;
}

message ListTaskRequest {

    // Parent is a fully qualified string that contains information about the
    // owner in hierarchical manner group/location/business (required)
    string parent = 1 [(validate.rules).string.min_len = 3];

    // First specifies the number of rows that are to be returned starting after
    // the the cursor (value of after) (required if last is 0 and before is
    // empty).
    uint32 first = 2;

    // After takes any value as cursor to get the data after that point. Data
    // should be sorted on type of value specified for After For Ex. id, datetime,
    // name...
    //(required if last is 0 and before is empty).
    string after = 3;

    // Last specifies the number of rows that are to be returned ending before the
    // the cursor (value of before) (required if first is 0 and after is empty).
    uint32 last = 4;

    // Before takes any value as cursor to get the data before that point. Data
    // should be sorted on type of value specified for Before For Ex. id,
    // datetime, name...
    //(required if first is 0 and after is empty).
    string before = 5;

    // ViewMask is the list of fields that need to be returned. Defaults to all.
    google.protobuf.FieldMask view_mask = 6 [(validate.rules).message.required = true];
}

message ListTaskResponse {
    // List of customers
    repeated TaskNode nodes = 1 [(graphql.field_name) = "edges"];
    // PageInfo contains information about the current page
    saastack.types.PageInfo page_info = 2;
}

// CustomerNode is an object with customer its position in the list
message TaskNode {
    string position = 1 [(graphql.field_name) = "cursor"];
    // Node is the actual customer object
    Task node = 2;
}

message UpdateTaskStatusRequest {
    // id of the task to be updated
    string id = 1 [(validate.rules).string.min_len = 3];
    // updated status
    Status status = 2 [(validate.rules).enum.defined_only = true];
}

message ListTaskByTitleRequest {

    // Parent is a fully qualified string that contains information about the
    // owner in hierarchical manner group/location/business (required)
    string parent = 1 [(validate.rules).string.min_len = 3];
    // List tasks with the given title.
    string title = 2 [(validate.rules).string = {min_len : 1, max_len : 100}];
    google.protobuf.FieldMask view_mask = 4 [(validate.rules).message.required = true];
}

message ListTaskByStatusRequest {
    // Parent is a fully qualified string that contains information about the
    // owner in hierarchical manner group/location/business (required)
    string parent = 1 [(validate.rules).string.min_len = 3];
    // list tasks with the given status
    Status status = 2 [(validate.rules).enum.defined_only = true];
    google.protobuf.FieldMask view_mask = 4 [(validate.rules).message.required = true];
}

message ListTaskByDateRequest {
    // Parent is a fully qualified string that contains information about the
    // owner in hierarchical manner group/location/business (required)
    string parent = 1 [(validate.rules).string.min_len = 3];
    // list tasks with given startDate
    .google.protobuf.Timestamp start_date = 2 [(validate.rules).timestamp.required = true];
    .google.protobuf.Timestamp due_date = 3 [(validate.rules).timestamp.required = true];
    google.protobuf.FieldMask view_mask = 4 [(validate.rules).message.required = true];
}

message AssignTaskRequest {
    // task id in which employees are needed to be assigned
    string id = 1 [(validate.rules).string.min_len = 3];
    // ids of employee which are to be assigned to the task
    repeated string employee_id = 2 [(validate.rules).repeated.min_items = 1];
}

message GetTaskReportRequest {
    string parent = 1;
    .google.protobuf.Timestamp start_date = 2 [(validate.rules).timestamp.required = true];
    .google.protobuf.Timestamp due_date = 3 [(validate.rules).timestamp.required = true];
}

message GetTaskReportResponse {
    repeated TaskReportResponse data = 1;
}

message TaskReportResponse {
    google.protobuf.Timestamp date = 1;
    int64 count = 2;
}

//
//message GetTaskActivityDetailsRequest {
//    string id = 1 [(validate.rules).string.min_len = 3];
//}
//
//message GetTaskActivityResponse {
//    repeated string data = 1;
//}




///////////// ------------------------------------ Roles And Rights ---------------------------------- /////////////////

option (pehredaar.module_roles).module_role = {
    module_role_name : "Viewer";
    display_name : "Viewer";
    rpc : "GetTask";
    rpc : "ListTask"
    rpc : "ListTaskByTitle";
    rpc : "ListTaskByStatus";
    rpc : "ListTaskByDate";
    rpc : "GetTaskReport";
};

option (pehredaar.module_roles).module_role = {
    module_role_name : "Editor";
    display_name : "Editor";
    rpc : "UpdateTask";
    rpc : "UpdateTaskStatus";
    rpc : "AssignTask";
    rpc : "GetTask";
    rpc : "ListTask"
    rpc : "ListTaskByTitle";
    rpc : "ListTaskByStatus";
    rpc : "ListTaskByDate";
    rpc : "GetTaskReport";
};

option (pehredaar.module_roles).module_role = {
    module_role_name : "Admin";
    display_name : "Admin";
    pattern : "{parent}**/.*"
};

////////////// ------------------------------------- Parent --------------------------------------- /////////////

service ParentService {
    rpc ValidateParent (ValidateParentRequest) returns (ValidateParentResponse);
}

message ValidateParentRequest {
    option (graphql.skip) = true;
    string id = 1;
}

message ValidateParentResponse {
    option (graphql.skip) = true;
    bool valid = 1;
}
