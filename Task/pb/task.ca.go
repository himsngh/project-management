package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	TasksCreateTaskActivity       = "/saastack.task.v1.Tasks/CreateTask"
	TasksGetTaskActivity          = "/saastack.task.v1.Tasks/GetTask"
	TasksDeleteTaskActivity       = "/saastack.task.v1.Tasks/DeleteTask"
	TasksUpdateTaskActivity       = "/saastack.task.v1.Tasks/UpdateTask"
	TasksListTaskActivity         = "/saastack.task.v1.Tasks/ListTask"
	TasksUpdateTaskStatusActivity = "/saastack.task.v1.Tasks/UpdateTaskStatus"
	TasksAssignTaskActivity       = "/saastack.task.v1.Tasks/AssignTask"
	TasksListTaskByTitleActivity  = "/saastack.task.v1.Tasks/ListTaskByTitle"
	TasksListTaskByStatusActivity = "/saastack.task.v1.Tasks/ListTaskByStatus"
	TasksListTaskByDateActivity   = "/saastack.task.v1.Tasks/ListTaskByDate"
	TasksGetTaskReportActivity    = "/saastack.task.v1.Tasks/GetTaskReport"
)

func RegisterTasksActivities(cli TasksClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.CreateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksCreateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.GetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteTaskRequest) (*Empty, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.DeleteTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksDeleteTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTaskStatusRequest) (*Task, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateTaskStatus(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateTaskStatusActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *AssignTaskRequest) (*Empty, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.AssignTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksAssignTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByTitleRequest) (*ListTaskResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTaskByTitle(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByTitleActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByStatusRequest) (*ListTaskResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTaskByStatus(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByStatusActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByDateRequest) (*ListTaskResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTaskByDate(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByDateActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskReportRequest) (*GetTaskReportResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.GetTaskReport(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskReportActivity},
	)
}

// TasksActivitiesClient is a typesafe wrapper for TasksActivities.
type TasksActivitiesClient struct {
}

// NewTasksActivitiesClient creates a new TasksActivitiesClient.
func NewTasksActivitiesClient(cli TasksClient) TasksActivitiesClient {
	RegisterTasksActivities(cli)
	return TasksActivitiesClient{}
}

func (ca *TasksActivitiesClient) CreateTask(ctx workflow.Context, in *CreateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksCreateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTask(ctx workflow.Context, in *GetTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) DeleteTask(ctx workflow.Context, in *DeleteTaskRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TasksDeleteTaskActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateTask(ctx workflow.Context, in *UpdateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTask(ctx workflow.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateTaskStatus(ctx workflow.Context, in *UpdateTaskStatusRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateTaskStatusActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) AssignTask(ctx workflow.Context, in *AssignTaskRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TasksAssignTaskActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByTitle(ctx workflow.Context, in *ListTaskByTitleRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByTitleActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByStatus(ctx workflow.Context, in *ListTaskByStatusRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByStatusActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByDate(ctx workflow.Context, in *ListTaskByDateRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByDateActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTaskReport(ctx workflow.Context, in *GetTaskReportRequest) (*GetTaskReportResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskReportActivity, in)
	var result GetTaskReportResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity = "/saastack.task.v1.ParentService/ValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
