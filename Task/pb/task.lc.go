// Code generated by protoc-gen-lc, DO NOT EDIT.

package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.opencensus.io/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type localTasksClient struct {
	TasksServer
}

func NewLocalTasksClient(s TasksServer) TasksClient {
	return &localTasksClient{s}
}

type traceTasksServer struct {
	TasksServer
}

func NewTraceTasksServer(in TasksServer) TasksServer {
	return &traceTasksServer{in}
}

func (lc *localTasksClient) CreateTask(ctx context.Context, in *CreateTaskRequest, opts ...grpc.CallOption) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.CreateTask(ctx, in)
}

func (s *traceTasksServer) CreateTask(ctx context.Context, in *CreateTaskRequest) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.CreateTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.CreateTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) GetTask(ctx context.Context, in *GetTaskRequest, opts ...grpc.CallOption) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.GetTask(ctx, in)
}

func (s *traceTasksServer) GetTask(ctx context.Context, in *GetTaskRequest) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.GetTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.GetTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) DeleteTask(ctx context.Context, in *DeleteTaskRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.DeleteTask(ctx, in)
}

func (s *traceTasksServer) DeleteTask(ctx context.Context, in *DeleteTaskRequest) (*empty.Empty, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.DeleteTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.DeleteTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) UpdateTask(ctx context.Context, in *UpdateTaskRequest, opts ...grpc.CallOption) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.UpdateTask(ctx, in)
}

func (s *traceTasksServer) UpdateTask(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.UpdateTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.UpdateTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) ListTask(ctx context.Context, in *ListTaskRequest, opts ...grpc.CallOption) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.ListTask(ctx, in)
}

func (s *traceTasksServer) ListTask(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.ListTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.ListTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) UpdateTaskStatus(ctx context.Context, in *UpdateTaskStatusRequest, opts ...grpc.CallOption) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.UpdateTaskStatus(ctx, in)
}

func (s *traceTasksServer) UpdateTaskStatus(ctx context.Context, in *UpdateTaskStatusRequest) (*Task, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.UpdateTaskStatus")
	defer span.End()
	// method call
	res, err := s.TasksServer.UpdateTaskStatus(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) AssignTask(ctx context.Context, in *AssignTaskRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.AssignTask(ctx, in)
}

func (s *traceTasksServer) AssignTask(ctx context.Context, in *AssignTaskRequest) (*empty.Empty, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.AssignTask")
	defer span.End()
	// method call
	res, err := s.TasksServer.AssignTask(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) ListTaskByTitle(ctx context.Context, in *ListTaskByTitleRequest, opts ...grpc.CallOption) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.ListTaskByTitle(ctx, in)
}

func (s *traceTasksServer) ListTaskByTitle(ctx context.Context, in *ListTaskByTitleRequest) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.ListTaskByTitle")
	defer span.End()
	// method call
	res, err := s.TasksServer.ListTaskByTitle(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) ListTaskByStatus(ctx context.Context, in *ListTaskByStatusRequest, opts ...grpc.CallOption) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.ListTaskByStatus(ctx, in)
}

func (s *traceTasksServer) ListTaskByStatus(ctx context.Context, in *ListTaskByStatusRequest) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.ListTaskByStatus")
	defer span.End()
	// method call
	res, err := s.TasksServer.ListTaskByStatus(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) ListTaskByDate(ctx context.Context, in *ListTaskByDateRequest, opts ...grpc.CallOption) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.ListTaskByDate(ctx, in)
}

func (s *traceTasksServer) ListTaskByDate(ctx context.Context, in *ListTaskByDateRequest) (*ListTaskResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.ListTaskByDate")
	defer span.End()
	// method call
	res, err := s.TasksServer.ListTaskByDate(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

func (lc *localTasksClient) GetTaskReport(ctx context.Context, in *GetTaskReportRequest, opts ...grpc.CallOption) (*GetTaskReportResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.TasksServer.GetTaskReport(ctx, in)
}

func (s *traceTasksServer) GetTaskReport(ctx context.Context, in *GetTaskReportRequest) (*GetTaskReportResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.Tasks")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.Tasks.GetTaskReport")
	defer span.End()
	// method call
	res, err := s.TasksServer.GetTaskReport(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}

type localParentServiceClient struct {
	ParentServiceServer
}

func NewLocalParentServiceClient(s ParentServiceServer) ParentServiceClient {
	return &localParentServiceClient{s}
}

type traceParentServiceServer struct {
	ParentServiceServer
}

func NewTraceParentServiceServer(in ParentServiceServer) ParentServiceServer {
	return &traceParentServiceServer{in}
}

func (lc *localParentServiceClient) ValidateParent(ctx context.Context, in *ValidateParentRequest, opts ...grpc.CallOption) (*ValidateParentResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}
	return lc.ParentServiceServer.ValidateParent(ctx, in)
}

func (s *traceParentServiceServer) ValidateParent(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	if in == nil {
		return nil, status.Error(codes.InvalidArgument, "request cannot be null")
	}

	ctx = ctxzap.ToContext(ctx, ctxzap.Extract(ctx).With(zap.String("service.name", ".saastack.task.v1.ParentService")))
	ctx, span := trace.StartSpan(ctx, ".saastack.task.v1.ParentService.ValidateParent")
	defer span.End()
	// method call
	res, err := s.ParentServiceServer.ValidateParent(ctx, in)
	st, _ := status.FromError(err)
	span.SetStatus(trace.Status{Code: int32(st.Code()), Message: st.Message()})

	return res, err
}
